CC=clang
LD=ld
FORMAT=clang-format
CFLAGS=-std=c99 -O2 -Wall -Wextra -pedantic
BUILD_DIR=./obj_files
SRC_DIR=./src
HEADERS_DIR=${SRC_DIR}/headers
BIN=./bin/main

build:
	${CC} -o ${BUILD_DIR}/complex.o ${CFLAGS}  -c ${SRC_DIR}/complex.c
	${CC} -o ${BUILD_DIR}/screen.o ${CFLAGS}   -c ${SRC_DIR}/screen.c 
	${CC} -o ${BUILD_DIR}/mand_set.o ${CFLAGS} -c ${SRC_DIR}/mand_set.c 
	${CC} -o ${BUILD_DIR}/frame.o ${CFLAGS}    -c ${SRC_DIR}/frame.c 
	${CC} -o ${BIN} ${CFLAGS} -lpthread      ${SRC_DIR}/main.c ${BUILD_DIR}/complex.o ${BUILD_DIR}/screen.o ${BUILD_DIR}/mand_set.o ${BUILD_DIR}/frame.o

fmt:
	${FORMAT} --style=GNU ${SRC_DIR}/main.c > ${SRC_DIR}/main1.c
	${FORMAT} --style=GNU ${SRC_DIR}/frame.c > ${SRC_DIR}/frame1.c
	${FORMAT} --style=GNU ${SRC_DIR}/screen.c > ${SRC_DIR}/screen1.c
	${FORMAT} --style=GNU ${SRC_DIR}/complex.c > ${SRC_DIR}/complex1.c
	${FORMAT} --style=GNU ${SRC_DIR}/mand_set.c > ${SRC_DIR}/mand_set1.c

	${FORMAT} --style=GNU ${HEADERS_DIR}/frame.h > ${HEADERS_DIR}/frame1.h
	${FORMAT} --style=GNU ${HEADERS_DIR}/screen.h > ${HEADERS_DIR}/screen1.h
	${FORMAT} --style=GNU ${HEADERS_DIR}/complex.h > ${HEADERS_DIR}/complex1.h
	${FORMAT} --style=GNU ${HEADERS_DIR}/mand_set.h > ${HEADERS_DIR}/mand_set1.h

	rm -rf ${SRC_DIR}/main.c ${SRC_DIR}/frame.c ${SRC_DIR}/screen.c ${SRC_DIR}/mand_set.c ${SRC_DIR}/complex.c
	rm -rf ${HEADERS_DIR}/frame.h ${HEADERS_DIR}/screen.h ${HEADERS_DIR}/complex.h ${HEADERS_DIR}/mand_set.h

	mv ${SRC_DIR}/main1.c ${SRC_DIR}/main.c
	mv ${SRC_DIR}/frame1.c ${SRC_DIR}/frame.c
	mv ${SRC_DIR}/screen1.c ${SRC_DIR}/screen.c
	mv ${SRC_DIR}/complex1.c ${SRC_DIR}/complex.c
	mv ${SRC_DIR}/mand_set1.c ${SRC_DIR}/mand_set.c

	mv ${HEADERS_DIR}/frame1.h ${HEADERS_DIR}/frame.h
	mv ${HEADERS_DIR}/screen1.h ${HEADERS_DIR}/screen.h
	mv ${HEADERS_DIR}/complex1.h ${HEADERS_DIR}/complex.h
	mv ${HEADERS_DIR}/mand_set1.h ${HEADERS_DIR}/mand_set.h

clean:
	rm -rf ${BUILD_DIR}/*

run: build
	${BIN}
