#include <stdio.h>

typedef struct
{
  double a;
  double b;
} Complex;

Complex
complex (double a, double b)
{
  Complex z = { a, b };

  return z;
}

Complex
null_complex (void)
{
  return complex (0.0f, 0.0f);
}

Complex
add (Complex z1, Complex z2)
{
  return complex (z1.a + z2.a, z1.b + z2.b);
}

Complex
mul (Complex z1, Complex z2)
{
  return complex (z1.a * z2.a - z1.b * z2.b, z1.a * z2.b + z2.a * z1.b);
}

void
print_complex (Complex z)
{
  printf ("a: %f b: %f\n", z.a, z.b);
}
