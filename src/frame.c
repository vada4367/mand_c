#include "headers/mand_set.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>

// Print mand set
void
frame (double i, double X, double Y)
{
  // Console size
  struct winsize win;
  ioctl (0, TIOCGWINSZ, &win);

  int w = win.ws_row;
  int h = win.ws_col;
  int THREADS = h;

  // Arrays of threads and arguments
  // for mandelbrot_set() function
  MandSetArg args[THREADS];
  pthread_t threads[THREADS];

  // Alloc output and part of output
  // (one part (line) one thread)
  char *part_mand = malloc (sizeof (char) * w * h / THREADS);
  char *mand_set = malloc (sizeof (char) * w * h);
  Screen screen = new_screen (h, w, X, Y, i);

  // Create arguments for mandelbrot_set()
  for (int j = 0; j < THREADS; j++)
    args[j] = new_arg (screen, w * h / THREADS * j, w * h / THREADS * (j + 1));

  // Math parts
  for (int j = 0; j < THREADS; j++)
    pthread_create (&threads[j], NULL, mandelbrot_set, (void *)&args[j]);

  // Wait end of maths and
  // join to result
  for (int j = 0; j < THREADS; j++)
    {
      pthread_join (threads[j], (void **)&part_mand);

      for (int k = 0; k < w * h / THREADS; k++)
        mand_set[(j * w * h / THREADS) + k] = part_mand[k];
    }

  // Print result
  printf ("\x1B[H");
  for (int i = 0; i < w * h; i++)
    {
      printf ("%c", mand_set[i]);

      if (i % h == 0)
        printf ("\n");
    }
}
