
typedef struct
{
  double a;
  double b;
} Complex;

Complex complex (double a, double b);

Complex null_complex (void);

Complex add (Complex z1, Complex z2);

Complex mul (Complex z1, Complex z2);

void print_complex (Complex z);
