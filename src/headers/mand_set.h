
#include "screen.h"

typedef struct
{
  Screen scr;
  int start;
  int end;
} MandSetArg;

MandSetArg new_arg (Screen scr, int start, int end);

void *mandelbrot_set (void *args);
