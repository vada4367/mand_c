
#include "complex.h"

typedef struct
{
  int w;
  int h;
  Complex *screen;
} Screen;

Screen new_screen (int w, int h, double dotx, double doty, double attitude);
