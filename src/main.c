#define _GNU_SOURCE
#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <termios.h>
#include "headers/frame.h"

int
wasd (int *input_move);

int
main (void)
{
  double X = 0.0f;
  double Y = 0.0f;

  int *input = malloc(sizeof(int) * 2);

  struct termios old, new1;

  tcgetattr (STDIN_FILENO, &old);
  new1 = old;
  new1.c_lflag &= (~ICANON & ~ECHO);
  tcsetattr (STDIN_FILENO, TCSANOW, &new1);

  double scale = 1.0f;

  frame (scale, X, Y);
  for (;;)
    {
      usleep (1000);

      wasd (input);

      if (input[0] || input[1])
        {
          if ((input[0] == 1 && input[1] == 1) || (input[0] == -1 && input[1] == -1))
            {
              if (input[0] == 1 && input[1] == 1) 
                scale *= 0.9;
              if (input[0] == -1 && input[1] == -1)
                scale *= (1/0.9);
            }
          else
            {
              X += input[0] * scale;
              Y += input[1] * scale;
            }

          frame (scale, X, Y);
        }
    }

  return 0;
}



char
sleep_input (void)
{
  struct timeval tv = { 0, 0 };
  fd_set fds;

  FD_ZERO (&fds);
  FD_SET (STDIN_FILENO, &fds);
  select (STDIN_FILENO + 1, &fds, NULL, NULL, &tv);
  if (FD_ISSET (STDIN_FILENO, &fds))
    {
      char input = getchar ();
      return input;
    }

  return '\0';
}

int
wasd (int *input_move)
{
  char wasd = sleep_input();

  input_move[0] = 0;
  input_move[1] = 0;

  switch (wasd)
    {
    case 'w':
      input_move[0] = 0;
      input_move[1] = 1;
      return 1;

    case 'a':
      input_move[0] = -1;
      input_move[1] = 0;
      return 1;

    case 's':
      input_move[0] = 0;
      input_move[1] = -1;
      return 1;

    case 'd':
      input_move[0] = 1;
      input_move[1] = 0;
      return 1;
    case '+':
      input_move[0] = 1;
      input_move[1] = 1;
      return 1;
    case '-':
      input_move[0] = -1;
      input_move[1] = -1;
      return 1;
    }

  return 0;
}
