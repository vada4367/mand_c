#include "headers/screen.h"
#include <stdlib.h>

const int ITERS = 100;

typedef struct
{
  Screen scr;
  int start;
  int end;
} MandSetArg;

MandSetArg
new_arg (Screen scr, int start, int end)
{
  MandSetArg arg = { scr, start, end };
  return arg;
}

// Math mandelbrot_set
void *
mandelbrot_set (void *args)
{
  MandSetArg func_args = *((MandSetArg *)args);
  int start = func_args.start;
  Screen scr = func_args.scr;
  int end = func_args.end;

  char *result = malloc (sizeof (char) * (end - start));

  for (int i = start; i < end; i++)
    {
      Complex c = scr.screen[i];
      Complex z = c;

      int count = 0;
      while (count++ < ITERS && !(z.a * z.a + z.b * z.b > 4.0f))
        z = add (mul (z, z), c);

      count > ITERS ? (result[i - start] = '#') : (result[i - start] = ' ');
    }

  return (void *)result;
}
