#include "headers/complex.h"
#include <stdlib.h>

typedef struct
{
  int w;
  int h;

  Complex *screen;
} Screen;

Screen
new_screen (int w, int h, double dotx, double doty, double attitude)
{
  Screen scr;

  scr.w = w;
  scr.h = h;

  scr.screen = malloc (sizeof (Complex) * (scr.w * scr.h + 1));

  for (int i = -w / 2; i < w / 2; i++)
    for (int j = -h / 2; j < h / 2; j++)
      scr.screen[i + w / 2 + (j + h / 2) * w]
          = complex (dotx + i * attitude, -doty + j * attitude);

  return scr;
}
